package main

import (
	"bytes"
	"fmt"
	"image"
	"image/draw"
	_ "image/gif"
	"image/jpeg"
	"image/png"
	"log"

	"golang.org/x/image/bmp"
	"golang.org/x/image/tiff"
)

func decodeImage(buf *bytes.Buffer) (*image.RGBA, string) {

	/*fo, err := os.Create("output2.jpg")
	  fo.Write(buf.Bytes())
	  fo.Close()*/

	img, imageType, err := image.Decode(buf)
	if err != nil {
		log.Fatal("decode failed ", err)
	}
	imgRGBA := image.NewRGBA(img.Bounds())
	draw.Draw(imgRGBA, imgRGBA.Bounds(), img, image.Point{0, 0}, draw.Src)

	fmt.Println("decode ok, imageType:", imageType)

	return imgRGBA, imageType
}

func encodeImage(img *image.Gray, format string, quality int) *bytes.Buffer {
	buf := new(bytes.Buffer)
	var err error
	switch format {
	case "jpeg":
		err = jpeg.Encode(buf, img, &jpeg.Options{Quality: 95})
	case "png":
		err = png.Encode(buf, img)
	case "tiff":
		err = tiff.Encode(buf, img, &tiff.Options{})
	case "bmp":
		err = bmp.Encode(buf, img)
	}

	if err != nil {
		log.Fatal("encode failed ", err)
	}

	return buf
}
