package main

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"math"
	"math/rand"
	"net"
	"os"
	"strconv"
	"time"
)

const (
	HOST                  = "localhost"
	PORT                  = "3333"
	REQ_HEADER_LENGTH     = 8
	RES_HEADER_LENGTH     = 22
	PROGRESS_STATE_LENGTH = 8
)

type ProgressState struct {
	tilesDone  int
	tilesTotal int
	progress   float32
}

type RequestHeader struct {
	no_req    uint16
	file_size uint32
	nb_tiles  uint16
}

type ResponseHeader struct {
	no_req          uint16
	file_size       uint32
	decode_time_ms  uint32
	sobel_time_ms   uint32
	encode_time_ms  uint32
	process_time_ms uint32
}

func parseArguments(args []string) (string, uint16, string) {
	if len(args) < 4 {
		log.Fatal("Usage: " + args[0] + " <image file> <nb tiles> <output image file | 'bench'>")
	}

	nb_tiles_parsed, err := strconv.ParseUint(args[2], 10, 16)
	if err != nil {
		log.Fatal("Failed reading thread number:", err.Error())
	}
	nb_tiles := uint16(nb_tiles_parsed)

	return args[1], nb_tiles, args[3]
}

func prepareRequest(inputFilePath string, nb_tiles uint16, outputFilePath string) ([]byte, []byte, *os.File) {
	inputFile, err := os.Open(inputFilePath)
	if err != nil {
		log.Fatal("Failed reading file:", err.Error())
	}

	file_infos, err := inputFile.Stat()
	if err != nil {
		log.Fatal("Failed to retrieve file stats:", err.Error())
	}
	file_size := uint32(file_infos.Size())

	inputFileBytes := make([]byte, file_size)
	inputFile.Read(inputFileBytes)
	inputFile.Close()

	var outputFile *os.File
	if outputFilePath != "bench" { // dont create output when benchmarking
		outputFile, err = os.Create(outputFilePath)
		if err != nil {
			log.Fatal("Error creating file:", err.Error())
		}
	}

	rand_generator := rand.New(rand.NewSource(time.Now().UnixNano()))
	no_req := uint16(rand_generator.Uint32())

	fmt.Println("Request number:", no_req)
	fmt.Println("File size:", file_size)
	fmt.Println("Number of tiles:", nb_tiles)

	// Build request header
	headerBytes := buildRequestHeader(&RequestHeader{
		no_req:    no_req,
		file_size: file_size,
		nb_tiles:  nb_tiles,
	})

	return headerBytes, inputFileBytes, outputFile
}

func request(conn net.Conn, headerBytes []byte, inputFileBytes []byte, outputfile *os.File) *ResponseHeader {
	// Send header and file
	tx := bufio.NewWriter(conn)

	tx.Write(headerBytes)    // send request header
	tx.Write(inputFileBytes) // send request header
	tx.Flush()               // must flush to make io.Copy synchronous

	// Listen for responses until we get the result
	rx := bufio.NewReader(conn)
	var resHeader *ResponseHeader
	for resHeader == nil {
		resType := make([]byte, 1)
		io.ReadFull(rx, resType)

		switch resType[0] {
		case 1: // it's the result
			resHeaderBytes := make([]byte, RES_HEADER_LENGTH)

			_, err := io.ReadFull(rx, resHeaderBytes)
			if err != nil {
				log.Fatal("Error getting header bytes: ", err.Error())
			}

			resHeader = parseResponseHeader(resHeaderBytes)
		case 2: // it's a progress packet (loading)
			psBytes := make([]byte, PROGRESS_STATE_LENGTH)
			io.ReadFull(rx, psBytes)
			ps := parseProgressState(psBytes)
			printLoading(ps)
		}
	}

	// Write output file
	imageBytes := make([]byte, resHeader.file_size)
	_, err := io.ReadFull(rx, imageBytes)
	if err != nil {
		log.Fatal("Error getting image bytes: ", err.Error())
	}

	if outputfile != nil { // outputfile is nil in benchmark mode
		outputfile.Write(imageBytes)
		outputfile.Close()
	}

	return resHeader
}

func openConnection() net.Conn {
	conn, err := net.Dial("tcp", HOST+":"+PORT)
	if err != nil {
		log.Fatal("Dial failed:", err.Error())
	}
	return conn
}

func main() {
	inputFilePath, nb_tiles, outputFilePath := parseArguments(os.Args)
	headerBytes, inputFileBytes, outputFile := prepareRequest(inputFilePath, nb_tiles, outputFilePath)

	if outputFile == nil { // Benchmark mode
		fmt.Println("===BENCHMARKING===")

		csv_file, err := os.Create(inputFilePath + ".csv")
		csv_file.WriteString("tiles_count,time (ms)\n")
		if err != nil {
			log.Fatal("Failed to create csv file", err.Error())
		}

		inputFileLen := uint32(len(inputFileBytes))
		for i := uint16(1); i <= nb_tiles; i++ {
			headerBytesBench := buildRequestHeader(&RequestHeader{
				no_req:    i,
				file_size: inputFileLen,
				nb_tiles:  i,
			})

			conn := openConnection()

			time := request(conn, headerBytesBench, inputFileBytes, nil).sobel_time_ms
			fmt.Printf("Sobel %d : %d ms\n", i, time)
			s := fmt.Sprintf("%d,%d\n", i, time)
			csv_file.WriteString(s)

			conn.Close()
		}

		csv_file.Close()

	} else { // Normal mode
		conn := openConnection()

		resHeader := request(conn, headerBytes, inputFileBytes, outputFile)

		// Print response header time infos
		fmt.Printf("Decode : %d ms\n", resHeader.decode_time_ms)
		fmt.Printf("Sobel : %d ms\n", resHeader.sobel_time_ms)
		fmt.Printf("Encode : %d ms\n", resHeader.encode_time_ms)
		fmt.Printf("Total : %d ms\n", resHeader.process_time_ms)

		fmt.Println("Closing connection")

		conn.Close()
	}
}

func printLoading(ps *ProgressState) {
	frames := []rune{'⠋', '⠙', '⠹', '⠸', '⠼', '⠴', '⠦', '⠧', '⠇', '⠏'}

	var frame rune
	if ps.progress == 1 {
		frame = '✓'
	} else {
		frame = frames[ps.tilesDone%len(frames)]
	}

	fmt.Printf("\r%s %d/%d tiles (%.2f%%)",
		string(frame), // animation
		ps.tilesDone,
		ps.tilesTotal,
		ps.progress*100,
	)
	if ps.tilesDone == ps.tilesTotal {
		fmt.Println()
	}
}

func buildRequestHeader(reqHeader *RequestHeader) []byte {
	headerBytes := make([]byte, REQ_HEADER_LENGTH)

	binary.LittleEndian.PutUint16(headerBytes[0:], reqHeader.no_req)    // request number
	binary.LittleEndian.PutUint32(headerBytes[2:], reqHeader.file_size) // file size
	binary.LittleEndian.PutUint16(headerBytes[6:], reqHeader.nb_tiles)  // nb tiles

	return headerBytes
}

func parseResponseHeader(headerBytes []byte) *ResponseHeader {
	headerf := ResponseHeader{
		no_req:          binary.LittleEndian.Uint16(headerBytes[0:]),
		file_size:       binary.LittleEndian.Uint32(headerBytes[2:]),
		decode_time_ms:  binary.LittleEndian.Uint32(headerBytes[6:]),
		sobel_time_ms:   binary.LittleEndian.Uint32(headerBytes[10:]),
		encode_time_ms:  binary.LittleEndian.Uint32(headerBytes[14:]),
		process_time_ms: binary.LittleEndian.Uint32(headerBytes[18:]),
	}

	return &headerf
}

func parseProgressState(psBytes []byte) *ProgressState {
	psf := ProgressState{
		tilesDone:  int(binary.LittleEndian.Uint16(psBytes[0:])),
		tilesTotal: int(binary.LittleEndian.Uint16(psBytes[2:])),
		progress:   math.Float32frombits(binary.LittleEndian.Uint32(psBytes[4:])),
	}

	return &psf
}
