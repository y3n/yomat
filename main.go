package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	_ "net/http/pprof"
	"time"
)

const (
	HOST = "localhost"
	PORT = "3333"
)

func main() {
	// start server
	l, err := net.Listen("tcp", HOST+":"+PORT)
	if err != nil {
		log.Fatal("Error listening:", err.Error())
	}
	defer l.Close()

	fmt.Println("Listening on " + HOST + ":" + PORT)

	// debug server
	go func() {
		log.Println(http.ListenAndServe("localhost:6060", nil))
	}()

	for {
		// listen for an incoming connections
		conn, err := l.Accept()
		if err != nil {
			log.Fatal("Error accepting: ", err.Error())
		}
		// Handle connections in a new goroutine
		go handleConnection(conn)
	}
}

func handleConnection(conn net.Conn) {
	defer conn.Close()

	r := bufio.NewReader(conn)
	// header buffer
	reqHeaderBytes := make([]byte, REQ_HEADER_LENGTH)
	var reqHeader *RequestHeader
	var imageBuffer *bytes.Buffer
	var imageBytes []byte

	// get request header
	_, err := io.ReadFull(r, reqHeaderBytes)
	if err != nil || err == io.EOF {
		conn.Close()
		log.Fatal("Error buffering request:", err.Error())
	}

	// decode request header
	reqHeader = parseRequestHeader(reqHeaderBytes)

	fmt.Printf("Request number: %d\n", reqHeader.no_req)
	fmt.Println("File size:", reqHeader.file_size)
	fmt.Println("Number of tiles:", reqHeader.nb_tiles)

	// header received, now receiving image
	imageBytes = make([]byte, reqHeader.file_size)

	fmt.Println("waiting for image..")
	_, err = io.ReadFull(r, imageBytes)
	fmt.Println("image buffer received")
	if err != nil && err != io.EOF {
		conn.Close()
		log.Fatal("Error buffering request:", err.Error())
	}

	// get image
	imageBuffer = bytes.NewBuffer(imageBytes)

	fmt.Println("image_buffer length", imageBuffer.Len())

	processStart := time.Now().UnixNano()

	inputImage, imageFormat := decodeImage(imageBuffer)
	decodeTime := time.Now().UnixNano() - processStart

	psCallback := func(ps ProgressState) {
		conn.Write(buildProgressState(ps))
	}
	imageSobel := sobel_mt(reqHeader.nb_tiles, inputImage, psCallback)

	sobelTime := time.Now().UnixNano() - processStart - decodeTime

	imageEncoded := encodeImage(imageSobel, imageFormat, 95)
	encodeTime := time.Now().UnixNano() - processStart - decodeTime - sobelTime

	imageEncodedBytes := imageEncoded.Bytes()
	processTime := time.Now().UnixNano() - processStart

	fmt.Printf("decodeTime: %d\n", decodeTime/1e6)
	fmt.Printf("sobelTime: %d\n", sobelTime/1e6)
	fmt.Printf("encodeTime: %d\n", encodeTime/1e6)
	fmt.Printf("processTime: %d\n", processTime/1e6)

	resHeader := buildResponseHeader(&ResponseHeader{
		no_req:          reqHeader.no_req,
		file_size:       uint32(len(imageEncodedBytes)),
		decode_time_ms:  uint32(decodeTime / 1e6),
		sobel_time_ms:   uint32(sobelTime / 1e6),
		encode_time_ms:  uint32(encodeTime / 1e6),
		process_time_ms: uint32(processTime / 1e6),
	})
	conn.Write(resHeader)
	conn.Write(imageEncodedBytes)

	fmt.Println("closing connection")
}
