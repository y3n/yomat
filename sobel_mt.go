package main

import (
	"image"
)

func sobel_mt(nb_tiles uint16, inputImage *image.RGBA, psCallback func(ps ProgressState)) *image.Gray {
	type ImageResult struct {
		i      uint16
		pixels []uint8
	}

	// Results channel
	results := make(chan ImageResult, nb_tiles)

	// For now, tiles are stripes
	tile_height := inputImage.Bounds().Max.Y / int(nb_tiles)
	minX := 0
	maxX := inputImage.Bounds().Max.X

	// Generate goroutines
	for i := uint16(0); i < nb_tiles; i++ {
		minY := int(i) * tile_height
		maxY := int(i+1) * tile_height
		if i == nb_tiles-1 {
			maxY = inputImage.Bounds().Max.Y
		}

		println("+Tile", i, ": (", minX, minY, ":", maxX, maxY, ")")

		go func(i uint16) {
			pixels := sobelTile(inputImage, minX, minY, maxX, maxY)
			println("*Tile", i, ":", len(pixels))

			res := ImageResult{
				i,
				pixels,
			}

			results <- res
		}(i)
	}

	// Slice containing all the pixels of the final image
	all_pixels := make([]uint8, len(inputImage.Pix))

	// Assemble results into output image, as routines end one by one, in a non-predictable order
	tilesDone := 0
	tilesTotal := int(nb_tiles)
	for tilesDone < tilesTotal {
		res := <-results
		tile_start := int(res.i) * (inputImage.Bounds().Max.X) * tile_height

		println("-Tile", res.i, "@", tile_start, len(res.pixels))

		for i, pix := range res.pixels {
			all_pixels[tile_start+i] = pix
		}
		tilesDone++

		psCallback(ProgressState{
			tilesDone:  tilesDone,
			tilesTotal: tilesTotal,
			progress:   float32(tilesDone) / float32(nb_tiles),
		})

	}

	gsImageSobel := image.NewGray(inputImage.Rect)
	gsImageSobel.Pix = all_pixels
	return gsImageSobel
}
