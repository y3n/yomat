# Multithreaded Sobel filter TCP server

## Installation
Build client and server

    git clone https://gitlab.com/y3n/yomat.git
    cd yomat
    go get
    go build .
    cd client
    go build .
    cd ..

## Usage
    client <image file> <nb tiles> <output image file | 'bench'>

### Example
    ./client/client ./client/diamond.png 8 ./client/diamond.sobel.png
## Bench
You can run a benchmark for demo using

    ./client/client ./client/diamond.png 24 bench

It will run 24 tasks using 1 to 24 threads and write a CSV file with all timings
