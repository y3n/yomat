package main

import (
	"encoding/binary"
	"math"
)

const (
	REQ_HEADER_LENGTH     = 8
	RES_HEADER_LENGTH     = 22 // length without type
	PROGRESS_STATE_LENGTH = 8
)

type RequestHeader struct {
	no_req    uint16
	file_size uint32
	nb_tiles  uint16
}

type ResponseHeader struct {
	no_req          uint16
	file_size       uint32
	decode_time_ms  uint32
	sobel_time_ms   uint32
	encode_time_ms  uint32
	process_time_ms uint32
}

type ProgressState struct {
	tilesDone  int
	tilesTotal int
	progress   float32
}

func buildResponseHeader(resHeader *ResponseHeader) []byte {
	headerBytes := make([]byte, RES_HEADER_LENGTH+1) // length with type
	headerBytes[0] = 1
	binary.LittleEndian.PutUint16(headerBytes[1:], resHeader.no_req)           // request number
	binary.LittleEndian.PutUint32(headerBytes[3:], resHeader.file_size)        // file size
	binary.LittleEndian.PutUint32(headerBytes[7:], resHeader.decode_time_ms)   // decode_time_ms
	binary.LittleEndian.PutUint32(headerBytes[11:], resHeader.sobel_time_ms)   // sobel_time_ms
	binary.LittleEndian.PutUint32(headerBytes[15:], resHeader.encode_time_ms)  // encode_time_ms
	binary.LittleEndian.PutUint32(headerBytes[19:], resHeader.process_time_ms) // process_time_ms

	return headerBytes
}

func parseRequestHeader(headerBytes []byte) *RequestHeader {
	headerf := RequestHeader{
		no_req:    binary.LittleEndian.Uint16(headerBytes[0:]),
		file_size: binary.LittleEndian.Uint32(headerBytes[2:]),
		nb_tiles:  binary.LittleEndian.Uint16(headerBytes[6:]),
	}

	return &headerf
}

func buildProgressState(ps ProgressState) []byte {
	psBytes := make([]byte, PROGRESS_STATE_LENGTH+1)
	psBytes[0] = 2
	binary.LittleEndian.PutUint16(psBytes[1:], uint16(ps.tilesDone))  // tilesDone
	binary.LittleEndian.PutUint16(psBytes[3:], uint16(ps.tilesTotal)) // tilesLeft
	progressBits := math.Float32bits(ps.progress)
	binary.LittleEndian.PutUint32(psBytes[5:], progressBits) // progress (%)
	return psBytes
}
