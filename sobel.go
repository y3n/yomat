package main

import (
	"image"
	"math"
)

func sobelTile(img *image.RGBA, minX int, minY int, maxX int, maxY int) []uint8 {
	kernelX := [9][3]int{
		{-1, 0, 1},
		{-2, 0, 2},
		{-1, 0, 1},
	}

	kernelY := [9][3]int{
		{-1, -2, -1},
		{0, 0, 0},
		{1, 2, 1},
	}

	res := make([]uint8, (maxX-minX)*(maxY-minY))
	for y := minY; y < maxY; y++ {
		for x := minX; x < maxX; x++ {
			if x == 0 || y == 0 || x == img.Bounds().Max.X-1 || y == img.Bounds().Max.Y-1 {
				res[(y-minY)*maxX+(x-minX)] = 0
				continue
			}

			pixelX := (kernelX[0][0] * pixelAt(img, x-1, y-1)) +
				(kernelX[0][1] * pixelAt(img, x, y-1)) +
				(kernelX[0][2] * pixelAt(img, x+1, y-1)) +
				(kernelX[1][0] * pixelAt(img, x-1, y)) +
				(kernelX[1][1] * pixelAt(img, x, y)) +
				(kernelX[1][2] * pixelAt(img, x+1, y)) +
				(kernelX[2][0] * pixelAt(img, x-1, y+1)) +
				(kernelX[2][1] * pixelAt(img, x, y+1)) +
				(kernelX[2][2] * pixelAt(img, x+1, y+1))

			pixelY := (kernelY[0][0] * pixelAt(img, x-1, y-1)) +
				(kernelY[0][1] * pixelAt(img, x, y-1)) +
				(kernelY[0][2] * pixelAt(img, x+1, y-1)) +
				(kernelY[1][0] * pixelAt(img, x-1, y)) +
				(kernelY[1][1] * pixelAt(img, x, y)) +
				(kernelY[1][2] * pixelAt(img, x+1, y)) +
				(kernelY[2][0] * pixelAt(img, x-1, y+1)) +
				(kernelY[2][1] * pixelAt(img, x, y+1)) +
				(kernelY[2][2] * pixelAt(img, x+1, y+1))

			r := math.Sqrt(float64((pixelX * pixelX) + (pixelY * pixelY)))
			if r > 255 {
				r = 255
			}

			res[(y-minY)*maxX+(x-minX)] = uint8(r)
		}
	}
	return res
}

func pixelAt(img *image.RGBA, x int, y int) int {
	if x < 0 || y < 0 || x >= img.Bounds().Max.X || y >= img.Bounds().Max.Y {
		return 0
	}
	idx := (y*img.Bounds().Max.X*4 + x*4)
	lum := 0.299*float64(img.Pix[idx]) + 0.587*float64(img.Pix[idx+1]) + 0.114*float64(img.Pix[idx+2])
	return int(lum)
}
